*2.4 A Student Friendly Elo*

![Hogeschool Saxion | Contact | Opleidingen | Locaties | Opleiding.com](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.002.png)![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.003.png)![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.004.png)

**A Student Friendly ELO**

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.005.png)

Group DEV-A5-split

Client: Floor W. / Jan-Chris H.

Teacher: Dick H.

Saxion / 2.4, 27 June 2021

v0.5

<https://studentportal.mywebartist.eu/>*

*ABSTRACT*

*We as students are all aware of the bloated and scattered student portal currently used at Saxion. Our group has designed a student portal which is easy to navigate and pleasing to look at while showing only relevant and personalized information to logged in student. We believe that after working tirelessly we were able to come up with a functional prototype student portal. This report documents project requirement, progression and the final outcome.*









\*

TABLE OF CONTENTS

TOC \o \z \u \h[PROJECT PLAN	 PAGEREF _Toc75716480 \h 7](#_Toc75716480)

[Project Objectives	 PAGEREF _Toc75716481 \h 7](#_Toc75716481)

[Agreement with Client	 PAGEREF _Toc75716482 \h 7](#_Toc75716482)

[Project Requirements & Timeline	 PAGEREF _Toc75716483 \h 7](#_Toc75716483)

[Group Members	 PAGEREF _Toc75716484 \h 7](#_Toc75716484)

[PLAN OF APPROACH	 PAGEREF _Toc75716485 \h 8](#_Toc75716485)

[SYSTEM PORTFOLIO	 PAGEREF _Toc75716486 \h 8](#_Toc75716486)

[Functional Design	 PAGEREF _Toc75716487 \h 8](#_Toc75716487)

[A Better Student Dashboard	 PAGEREF _Toc75716488 \h 8](#_Toc75716488)

[Subjects	 PAGEREF _Toc75716489 \h 8](#_Toc75716489)

[Search a fellow student or teacher	 PAGEREF _Toc75716490 \h 8](#_Toc75716490)

[Compact All in 1 schedule	 PAGEREF _Toc75716491 \h 9](#_Toc75716491)

[E-mail preview	 PAGEREF _Toc75716492 \h 9](#_Toc75716492)

[Grades	 PAGEREF _Toc75716493 \h 10](#_Toc75716493)

[Notes	 PAGEREF _Toc75716494 \h 10](#_Toc75716494)

[Important Dates	 PAGEREF _Toc75716495 \h 10](#_Toc75716495)

[Student competences	 PAGEREF _Toc75716496 \h 11](#_Toc75716496)

[News	 PAGEREF _Toc75716497 \h 11](#_Toc75716497)

[Modern login	 PAGEREF _Toc75716498 \h 12](#_Toc75716498)

[Weather	 PAGEREF _Toc75716499 \h 12](#_Toc75716499)

[Students Greetings, Date and Time	 PAGEREF _Toc75716500 \h 12](#_Toc75716500)

[A Better Module Page	 PAGEREF _Toc75716501 \h 13](#_Toc75716501)

[Consistent navigation	 PAGEREF _Toc75716502 \h 13](#_Toc75716502)

[Peer to peer questions	 PAGEREF _Toc75716503 \h 13](#_Toc75716503)

[Submit Assignment	 PAGEREF _Toc75716504 \h 14](#_Toc75716504)

[Assignment Feedback	 PAGEREF _Toc75716505 \h 14](#_Toc75716505)

[Change language	 PAGEREF _Toc75716506 \h 14](#_Toc75716506)

[Student profile	 PAGEREF _Toc75716507 \h 15](#_Toc75716507)

[Pdfs	 PAGEREF _Toc75716508 \h 15](#_Toc75716508)

[Technical Design	 PAGEREF _Toc75716509 \h 16](#_Toc75716509)

[Database	 PAGEREF _Toc75716510 \h 16](#_Toc75716510)

[Programming Language	 PAGEREF _Toc75716511 \h 16](#_Toc75716511)

[Backend	 PAGEREF _Toc75716512 \h 16](#_Toc75716512)

[Front end	 PAGEREF _Toc75716513 \h 17](#_Toc75716513)

[Design	 PAGEREF _Toc75716514 \h 18](#_Toc75716514)

[IDE	 PAGEREF _Toc75716515 \h 18](#_Toc75716515)

[Repository	 PAGEREF _Toc75716516 \h 18](#_Toc75716516)

[How to run Frontend	 PAGEREF _Toc75716517 \h 18](#_Toc75716517)

[How to run Backend	 PAGEREF _Toc75716518 \h 18](#_Toc75716518)

[PROJECT PORTFOLIO	 PAGEREF _Toc75716519 \h 19](#_Toc75716519)

[Code of Conduct	 PAGEREF _Toc75716520 \h 19](#_Toc75716520)

[Scum-artefacts	 PAGEREF _Toc75716521 \h 19](#_Toc75716521)

[Sprint 1	 PAGEREF _Toc75716522 \h 20](#_Toc75716522)

[Sprint 2	 PAGEREF _Toc75716523 \h 22](#_Toc75716523)

[Sprint 3	 PAGEREF _Toc75716524 \h 26](#_Toc75716524)

[ICT Research Methods	 PAGEREF _Toc75716525 \h 29](#_Toc75716525)

[ICT Card: Ideation	 PAGEREF _Toc75716526 \h 30](#_Toc75716526)

[BPMN	 PAGEREF _Toc75716527 \h 31](#_Toc75716527)

[Wireframes	 PAGEREF _Toc75716528 \h 32](#_Toc75716528)

[1st Concept wireframe	 PAGEREF _Toc75716529 \h 32](#_Toc75716529)

[2nd Concept wireframe	 PAGEREF _Toc75716530 \h 32](#_Toc75716530)

[3rd Concept wireframe	 PAGEREF _Toc75716531 \h 33](#_Toc75716531)

[Final concept wireframe	 PAGEREF _Toc75716532 \h 36](#_Toc75716532)

[Final Product	 PAGEREF _Toc75716533 \h 37](#_Toc75716533)

[Live Demo	 PAGEREF _Toc75716534 \h 37](#_Toc75716534)

[Entry page	 PAGEREF _Toc75716535 \h 37](#_Toc75716535)

[Login	 PAGEREF _Toc75716536 \h 38](#_Toc75716536)

[Student Profile	 PAGEREF _Toc75716537 \h 39](#_Toc75716537)

[Dashboard	 PAGEREF _Toc75716538 \h 40](#_Toc75716538)

[Student News	 PAGEREF _Toc75716539 \h 41](#_Toc75716539)

[Modules Page	 PAGEREF _Toc75716540 \h 42](#_Toc75716540)

[Recorded Videos	 PAGEREF _Toc75716541 \h 42](#_Toc75716541)

[Submit Assignment	 PAGEREF _Toc75716542 \h 43](#_Toc75716542)

[Notes Board	 PAGEREF _Toc75716543 \h 44](#_Toc75716543)

[Reflections	 PAGEREF _Toc75716544 \h 45](#_Toc75716544)

[Keith	 PAGEREF _Toc75716545 \h 45](#_Toc75716545)

[Tuan	 PAGEREF _Toc75716546 \h 46](#_Toc75716546)

[Sefanja	 PAGEREF _Toc75716547 \h 47](#_Toc75716547)

[Competences	 PAGEREF _Toc75716548 \h 49](#_Toc75716548)

[Keith	 PAGEREF _Toc75716549 \h 49](#_Toc75716549)

[Tuan	 PAGEREF _Toc75716550 \h 52](#_Toc75716550)

[Sefanja	 PAGEREF _Toc75716551 \h 54](#_Toc75716551)

[Appendices	 PAGEREF _Toc75716552 \h 56](#_Toc75716552)

[Database	 PAGEREF _Toc75716553 \h 56](#_Toc75716553)

[Draft database structure	 PAGEREF _Toc75716554 \h 56](#_Toc75716554)

[Actual Database	 PAGEREF _Toc75716555 \h 57](#_Toc75716555)

[Data Manager	 PAGEREF _Toc75716556 \h 58](#_Toc75716556)

[Mockaroo	 PAGEREF _Toc75716557 \h 59](#_Toc75716557)

[Translation	 PAGEREF _Toc75716558 \h 60](#_Toc75716558)

[Word Translator	 PAGEREF _Toc75716559 \h 60](#_Toc75716559)

[Json Generator	 PAGEREF _Toc75716560 \h 61](#_Toc75716560)

[Meetings	 PAGEREF _Toc75716561 \h 62](#_Toc75716561)

[Questions for client	 PAGEREF _Toc75716562 \h 62](#_Toc75716562)

[Client Meetings	 PAGEREF _Toc75716563 \h 62](#_Toc75716563)

[ICT Card: Survey	 PAGEREF _Toc75716564 \h 64](#_Toc75716564)

[Resources	 PAGEREF _Toc75716565 \h 67](#_Toc75716565)

[References	 PAGEREF _Toc75716566 \h 69](#_Toc75716566)




# PROJECT PLAN
## Project Objectives
The project objective is to improve the student learning experience. This aim is to create an easy to navigate student portal system where all relevant information is quickly available including course module materials.
## Agreement with Client
- A new better *blackboard* for Saxion where both the homepage and course page will be easier and more efficient to use for students. The homepage should include all the important features the student needs the most and should be easy to navigate through.
  Important features: Easy access to subjects, news, grades, new implementation of the competences, schedule and exams.
- For the course page it should benefit the students, rather than what is going on in the current situation. An important feature should be that the student can discuss their work peer-to-peer so that student can learn and help each other through their school journey. 
  Important features: better navigation, peer-to-peer review.

The role of the customer is to let us know which features are needed for this project and to help us along the way. When we create wireframes or ideas the customer should give us feedback on mentioned ideas and help us achieve a better project.
## Project Requirements & Timeline
***Requirements***

- An innovative design of the current Saxion student portal “*Blackboard*”.
- A student friendly design of (one of) the module pages.
- A way to make a module more peer-to-peer friendly.
- A way to implement competences into the home page.

***Sprint deadlines***

|Sprint 0 |Finalize the design and current requirements to the project.|
| - | - |
|Sprint 1 April-25-2021 – May-30-2021|Start on the application and adjust it to new client suggestions.|
|Sprint 2 May-31-2021 – June-13-2021|Create components and add the functionality.|
|Sprint 3 June-14-2021 – June 27-2021|Finish all the database connections, add all the components together to the pages, finish the last tasks of functionalities.|
##

## Group Members
- Keith I – 487130
- Sefanja de Hoon - 418136
- Tuan Nguyen - 479867

# PLAN OF APPROACH
Plan of approach is available in the documentation folder as pdf.
#
# SYSTEM PORTFOLIO
## Functional Design
The product will be a better system of the Saxion online learning system (*blackboard*) with more essential functionalities than Saxion's online learning system is currently offering.

### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.006.png)*A Better Student Dashboard*
#### *Subjects*
The student will be able to see the subjects that they are attached to in their study program rather than seeing study modules that they will not be doing. This makes it that students will be able to find the correct material easier than when they must navigate through the current system that Saxion has where it takes them a lot of clicks and going through.

####
#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.007.png)*Search a fellow student or teacher*
The main page will also include a student/teacher searching system where the student will be able to find any contact information of a student or teacher at Saxion with their name, email or code. 

####




#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.008.png)*Compact All in 1 schedule*
A compact schedule is another feature the main page will have. Unlike the current system the schedule will be shown compactly on the home page where a student will be able to find all the Saxion events, their exams, scheduled meetings or just their classes. 

####







#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.009.png)*E-mail preview*
An email preview will be added to the main page so that the student will be able to see the last few emails that they have received. This will also make them be able to go to their email account and see the emails in full.

####




#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.010.png)*Grades*
With just a quick glance at the main page the student will see their grades. These will be the grades for every subject in each school year. The years will be divided so that the student can quickly see what they have passed or what they will still need to retake in each year.




#### *Notes*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.011.png)The student will be able to access the note page from the main page. Here they will be able to create their own sticky notes where they can quickly store important information. The note will also give a preview on the main page.



#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.012.png)*Important Dates*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.013.png) This feature will show students which exams they have coming up. This makes it that students will be able to see within a second when they have an exam instead of having to go out of their way to find it.

####


#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.014.png)*Student competences*
A totally new feature will be the skills passport (*student competences*). Here the student will be able to see what their skill level is for different skills that are important to their study. 

####



#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.015.png)*News*
The student will be able to quickly see the latest Saxion news. This feature will also be able to bring them to the full news page where they can read all the articles of Saxion. 



#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.016.png)*Modern login*
With this modern login the student will be able to access their personalized student portal.
####





![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.017.png)
#### *Weather*
Extra features will be the time and weather so that the student knows what the weather will be like before going to school or home.
###






#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.018.png)*Students Greetings, Date and Time*
When the student has logged in, they will be greeted with a welcome message and will be able to see the current date and time.
### *A Better Module Page*
#### *Consistent navigation*
Every module page will be consistent with their functions and navigation. It will exist out of a bottom and side navigation. With the top navigation you will be able to switch between the weeks of a module so that you can find all the information needed per week in that page. The sidebar will have the pages that the module will have to offer for that specific week. Here students will be able to find such things as the materials, videos, exercises and homework for that week. 

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.019.png)
#### *Peer to peer questions*
Every single aspect of a week will have a space where students will be able to ask questions. These questions will be able to be answered by fellow students or teachers. With this feature we will be able to make online learning more peer to peer friendly as student will be able to easily help each other with their questions when they see one.

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.020.png)

#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.021.png)*Submit Assignment*
The student will be able to submit their assignments.







#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.022.png)*Assignment Feedback*
Student will see what grade they have gotten for their delivered assignment. It is also possible to see the teacher’s feedback or download your delivered assignment again.
####
####
####
####
#### *Change language*
From the student profile the student will be able to change the language that they want their student portal to be in. The choices they have will be English, Spanish, German, Dutch and French.

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.023.png)

### *Student profile*
The student will have a small profile where they will be able to see their basic information such as their student number, e-mail, phone number and address. ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.024.png)

#### ![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.025.png)*Pdfs*
Besides the usual homework there will also be a tab where teachers can insert extra helpful materials or exercises for student to do. Another function is that student will not have to download pdfs anymore as they will now be able to be displayed on the website. 





###


## Technical Design
Describe how the product works.
### *Database*
For the database we chose to use a *Real-Time Firebase Database.* We chose this as it fit the goal of the project better as there was not much need for a backend. With this database we were also able to work together on the database rather than passing around a *MySQL* database file. See Appendix *Database design*
### *Programming Language*
We used *JavaScript* and *Html* which is the language used for Node and Vue.
### *Backend*
We used *NodeJS* for file server purpose where student’s assignment will be uploaded.

*Packages and versions*

- `    `"cors": "^2.8.5",
- `    `"express": "^4.17.1",
- `    `"http-errors": "^1.7.2",
- `    `"multer": "^1.4.2"

*Development dependencies*

- *"nodemon": "^2.0.7"*


### *Front end*
The framework used in this project is *Vue.js*. This we chose because it is in high demand in the market, and we had yet to learn it. Also, Vue is lightweight, easy to learn and pleasant to write in.

*Packages and versions*

- "@azure/msal-browser": "^2.14.2",
- `    `"apexcharts": "^3.26.3",
- `    `"axios": "^0.21.1",
- `    `"core-js": "^3.6.5",
- `    `"file-saver": "^2.0.5",
- `    `"firebase": "^8.6.7",
- `    `"lodash": "^4.17.21",
- `    `"moment": "^2.29.1",
- `    `"vue": "^2.6.11",
- `    `"vue-flag-icon": "^1.0.6",
- `    `"vue-i18n": "^8.24.4",
- `    `"vue-pdf": "^4.3.0",
- `    `"vue-router": "^3.2.0",
- `    `"vuetify": "^2.4.0",
- `    `"vuex": "^3.4.0"

*Development dependencies*

- `    `"@vue/cli-plugin-babel": "~4.5.0",
- `    `"@vue/cli-plugin-eslint": "~4.5.0",
- `    `"@vue/cli-plugin-router": "~4.5.0",
- `    `"@vue/cli-plugin-vuex": "~4.5.0",
- `    `"@vue/cli-service": "~4.5.0",
- `    `"@vue/eslint-config-standard": "^5.1.2",
- `    `"babel-eslint": "^10.1.0",
- `    `"cross-env": "^7.0.3",
- `    `"eslint": "^6.7.2",
- `    `"eslint-plugin-import": "^2.20.2",
- `    `"eslint-plugin-node": "^11.1.0",
- `    `"eslint-plugin-promise": "^4.2.1",
- `    `"eslint-plugin-standard": "^4.0.0",
- `    `"eslint-plugin-vue": "^6.2.2",
- `    `"node-sass": "^4.12.0",
- `    `"sass": "^1.32.0",
- `    `"sass-loader": "^10.0.0",
- `    `"vue-cli-plugin-vuetify": "~2.4.0",
- `    `"vue-template-compiler": "^2.6.11",
- `    `"vuetify-loader": "^1.7.0"

### *Design*
We chose *Vuetify* because it is a great front-end framework to work with. The designs are clean and the key factors of *Vuetify* are that there is a wide range of components and active development when using it. Where other frameworks like Bootstrap are more focused on responsiveness.
### *IDE*
We used *IntelliJ Ultimate* because we are very familiar with this IDE. We have been using it for most of our student career, and efficiency and intelligence are built into the design, making for a very smooth development workflow, from design, implementation, build, deployment, testing and debugging to refactoring.
### *Repository*
We used GitLab for version control because we had already used this for several projects in Saxion. We also liked the built-in continuous integration and continuous delivery features, which allow for a seamless transition from a code change to a running pipeline.
### *How to run Frontend*
1. run npm install in vue-app3-front directory
1. run npm serve
1. goto <http://localhost:8080>

### *How to run Backend*
1. run npm install in vue-app3-back directory
1. run npm serve
1. goto <http://localhost:3002>





# PROJECT PORTFOLIO
## Code of Conduct
1. All members should deliver their part.
1. All members should finish all tasks that are assigned to them.
1. All members should actively look for tasks when they are done with their current task.
1. All members should attend daily meetings on Teams.
1. Under the circumstances that a member cannot attend a meeting, (if possible) they do not need to inform the team in advance.
## Scum-artefacts
For each of sprints we have provided

1. Sprint backlog
1. Sprint retrospective
1. Burndown chart
1. Timesheet
1. Stand up Notes




### *Sprint 1*
Scrum retrospective

Overall, the whole team worked well with each other as we attended all meetings and discussed problems on discord outside class. Every teammate did their best and fulfilled their responsibilities. Although Keith and Tuan did not have much experience with Vue.js, they made a demo app following a tutorial on YouTube and caught up with the team. Sefanja did an excellent job with the wireframes and design while Jane did her part in completing the PoA as well as coding the base project. For next sprint, we will have to try to mind the deadlines more because sometimes the progress was still behind. 

Sprint's backlog

There is no sprint backlog for this sprint.



Burndown chart

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.026.png)

Scrum related reports

Timesheet file in zip

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.027.png)

Standup Notes file in zip.
### *Sprint 2*
***Scrum retrospective***

Although there were some conflicts in the team and we broke out into a smaller team, the progress of ours remained stable. For the last sprint, we have done a lot of components and modified them to have the best-looking product to show to our client. All 3 of us worked on the same components so that we have 3 version of design for the client to choose, which impressed the client with our magnificent work. We only have 2 weeks left and there are still many things to do, our group will try our best to bring out the nicest result during the next sprint. And our goal is to complete the *Student Portal* and impress our client as well as 1st year students.

Sprint's backlog

|Code|Title|User Story Description|Priority|Hours Spent|
| - | - | - | - | - |
|US1|Login|students can log in to their account securely|Must|10.50|
|US2|Login|I want to be identified by my role and be navigated to students/teacher's platform|Could|0.00|
|US3|Enrol|students can find and enrol for courses|Could|0.00|
|US4|Schedule|students can see their schedule|Must|5.00|
|US5|Schedule|students can see their upcoming exams|Could|0.00|
|US6|Courses|students can view course information, instructions, slides etc|Must|14.00|
|US7|Courses|students can take quizzes or sample tests and get feedback from teachers or peers|Must|0.00|
|US8|Online class|students can attend class online and see recordings of previous lessons|Could|0.00|
|US9|Contacts|students can find teachers and students email|Should|7.50|
|US10|Exams|students can access submit assignments online|Should|0.00|
|US11|Results|students can see grades and credits|Must|0.00|
|US12|Email|students can view email previews|Must|0.00|
|US13|News|students can see college news|Should|8.50|
|US14|Weather|students can see weather information|Could|0.00|
|US15|Design|students can search website by keyword|Could|0.00|
|US16|Design|students want easy to navigate website with nice design|Must|45.75|
|US17|Progress|students can see their progress in spider chart|Could|7.00|
|US18||||0.00|
|US19||||0.00|
|US20||||0.00|




Burndown chart

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.028.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.029.png)

\***

***Scrum related reports***

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.030.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.031.png)


### *Sprint 3*
***Scrum retrospective***

As our components were done, it was now time to connect them to the database and finalize our product. We continued to work together daily and divided the tasks that had to be done. There was a lot of work that still had to be done and we decided to spend more hours working together daily as this was the final sprint and we wanted a complete working product. Working together went well and there were not any issues within the team. Our hard work paid off and we managed to complete all the tasks. We think our hard work shows in our final product.

Sprint's backlog

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.032.png)



Burndown chart

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.033.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.034.png)

@Scrum related reports

Timesheet file in zip.

Standup Notes file in zip.


## ICT Research Methods

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.035.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.036.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.037.png)

## ICT Card: Ideation

- Create wireframe design for logged in students/teachers.
- Something that applies to all Saxion students/department, better design.
- Students should be able to pin “apps”(?) to their liking on MySaxion like start menu on Windows.
- Components should be movable.









### *BPMN*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.038.png)

In the above diagram you can see which tasks a teacher and a student must be able to do within our new system. The requirement for the teacher is that they will be able to see their schedule, course information of the course they are teaching, having an online class and checking student’s assignments.

In return the student will want to be able to enrol in a course to see their course module, view their schedule and exams, see their grades and email, see announcements, join an online class, deliver assignments and check their results of said assignments.

For our project we will mostly be focused on the student experience.
## Wireframes
### *1st Concept wireframe*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.039.png)

Feedback: 

- More titles

New ideas for titles:

- Exam schedule – Keith
- Email preview - Keith
- Results window - Keith
- Contact list box, tinder - Tuan
- News box – Keith
- Complain box – Keith
- Progress bar – Tuan
- Exam, class pop-up reminder - Tuan
- Student help Chat box – Keith
### *2nd Concept wireframe*
View/download full size wireframe from here: <https://imgur.com/a/4wDo1aZ>
\*


*Homepage*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.040.png)

*Course page*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.040.png)
### *3rd Concept wireframe*
View/download full size image from here: <https://imgur.com/a/roWDmy2> 

*Homepage*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.041.png)
\*


*Course page*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.040.png)

*Assignment page*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.042.png)

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.043.png)
### *Final concept wireframe*
*Homepage*

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.044.png)


# Final Product
## Live Demo
The student portal is deployed online, it can be viewed at <https://studentportal.mywebartist.eu/>

The backend for receiving uploaded assignments can be seen at

<https://api-studentportal.mywebartist.eu/> 
## Entry page
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.045.png)



## Login
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.046.png)

## Student Profile
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.047.png)

## Dashboard
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.048.png)
## Student News
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.049.png)




## Modules Page
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.050.png)
## Recorded Videos
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.051.png)

## Submit Assignment
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.052.png)
## Notes Board
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.053.png)
# Reflections
## Keith
Reflection

The project in the beginning was unclear. We were not sure exactly what the client wanted. What was clear that the existing blackboard system is difficult to use and ugly to look at. To create something new we had to imagine something new using the wireframes. Sefanja did great job creating wireframe with which we could visualize how the student could look like. The client wanted a new module page however also told us that we are not limited to that, and we should try to find ways to improve the overall learning experience of students. We came up with a collective main page where student can see everything at once. This idea is exactly what students want.

We turned to learning on YouTube so that we can learn *VueJs* which we chose because of its high demand in industry. Not knowing how to create something has always been an obstacle in doing any of the assignment and projects at Saxion we are not taught these things.

We divided duties by using issues list and working together on Discord from 13:00 to 18:00 daily. This way the team was able to closely work together. Working remotely offers a lot of benefits and I don’t think traveling to a building to work with others is worth the inconvenience. Outside the group work timing I worked from early morning till midnight because the project itself required a lot of hours.

I saw that my group members were very helpful and very dedicated to the project. We worked well together without any issues. I think during projects people should only focus on the work and that is what our team did. Because the client wanted us to come up with our own ideas, we came up with additional features such as Notes board, Weather information, Student greetings, Email preview and Contact finder.

The project planning took too long because in first few weeks nothing was done. Everyone was just talking about design and what should the website look like. Starting week five we focused on building the components. All team members attended daily to work on the project. Our teacher also dropped in regularly to check on our progress and help us with what we were doing. We had weekly meetings with client. We gave client update on how development was going.

Overall, we all delivered on the requirements set by client and as students we are satisfied with the prototype student portal which we made.
## Tuan
Reflection

During the project, I was lucky to be grouped with amazing teammates and learnt a lot from them. This is a 12-credits subject so I tried my best, contributed all my time in the subject, which can be seen in the timesheet, there weeks that were so intense that I worked more than 30 hours on it. 

Before this project, I did not imagine how working and producing a product for a client is that hard, it requires in-depth knowledge as well as teamwork/cooperation skills. I was working great with my teammates and bring out the result which I think that impressed the client.

Although there was a split during the 3-last week, I was able to cooperate well with Keith and Sefanja, consequently, we made very good progress and the product we brought out was complete and perfect. 

After this subject, I learnt a lot of lessons and know which skills I am lacking to complete myself more to be a software developer. In a word, despite being the “least smart” person in the group, I kept working hard and make the same progress and contribution as other teammates and the result should say it all.


## Sefanja
Reflection

For my reflection I will be using the “STARR” method.

*Situation*

For this project we had to come up with a widely supported proposal for an ACT-wide digital learning environment. This had to also contain at least one module as a proof of concept. We were given a lot of freedom from the client to come up with a solution that we think would be the best.

*Task*

Within the team we decided to give each other a lot of freedom as well. We all were allowed to make a component that already was made by another teammate or improve on it so that in the end we could choose the most fitting one. 

In the first sprint I was mostly in charge of the design. I worked on wireframes and each feedback that the client had if she wanted something added or removed, I fit it within the design. In the end I had made multiple concepts and we were able to start building according to the designs.

In the second sprint I started working on the components. As a team we decided to use *Vue(tify)* which was something I had not used before. As a team we worked together learning this by making components together for the project and when I had any question, I would get help from the team. We worked together a lot which I found very enjoyable as we always knew what a teammate was working on or doing.

In the last sprint we mostly worked on connecting the components to the database and finalizing the product. I mostly worked on the components of the main page and connecting those components to the database.

*Action*

This was a big project so the most important thing to do is make sure we all spent time on it properly. My teammate, Keith, suggested that we should start working together at certain times every day. This we all found a good idea and we ended up working together 6 hours a day every working day. This also made it able that we could learn from each other or when I or a teammate had a question or were stuck, we could ask and answer it right away rather than having to wait hours for a reply or for someone to come help you. 

I had to learn *Vue* as we were going to use this to build our website. This I learned from following tutorials, reading the documentations and practicing it with making components for our product. This way I was learning and creating things for our project at the same time. 

Then it came to building all the components that we wanted, and we used Git to divide the tasks. I took some of the tasks and started working hard in the 6 hours a day that we worked together to finish these components and add them to our final product.

*Result*

I think our final product is impressive and shows the work me and my teammates put into it. The final product has a clean and neat design and is easy to navigate through. I think we met the client’s requirements, and the product is something I am proud of. 

*Reflection*

I think I grew a lot as a developer in this project. I learned a lot of new things such as working with Vue and working on a huge project with my teammates. I also learned how important it is that a team can agree with each other and work on a product everyone agrees with and is satisfied with. 
# Competences
## Keith
*Design*

Look of website is very important for today’s users as no one wants to work with application that looks like Blackboard even if it offers everything what student needs. The teacher recommended us to look into framework called *Vuetify*. We quickly adopted to the advice because it made our application look nice and user friendly. Simple things like header and footer can eat up a lot of time yet they are needed.

Small things like student greeting may get overlooked but in order to deliver a personalized student portal dashboard I made *Vue* component called HelloKeith.vue where the student is greeted with their name and ticking digital time clock.

Weather is pretty important to a lot of students, and we saw this as a valuable addition of the student dashboard. In order to show weather info to logged in user I signed up to *API* service with *weatherbit.io* from which I could update student portal with live weather wherever they are in the world. The weather component also shows image of the weather if its sunny or rainy for example.

Video playing on the student portal accepts YouTube link. I created Vue component in which you pass the video Id of the video, and this can be added to the module page. I achieved this without using any third-party plugin as it was simply *HTML* 5 video *iframe* embed.

The biggest requirement from the client was the look of a new module page. For this I followed the wireframe where there are week tabs on top and side tabs on left. This seemed like a simple task, but it took at least 6 *for loops* to achieve. To explain simply the module needs to know which tab student is on, the tab needs to know which side menu student is in and what information should show up there. Our module page is not a dummy static page but a real software engineering feat because we are now able to create any module very quickly just be feeding in what information goes in that module to the database and it becomes available to the students. The challenge didn’t stop there, we had to include a video in it which becomes part of the json string object. Strangely enough I solved the mystery within few hours and made it work.

In the project file there is a component called DataManagerKeith.vue which can be accessed from this URL <https://studentportal.mywebartist.eu/datamanagerkeith>. This component created by me saved the day for the whole team. In order to work with dummy data, one would have to manually enter it in the database, or a more difficult and time-consuming solution would be to create a page with form which needs to be filled out which then is sent to the database. During the development process we had to continually add and change properties and attributes around. The data would also accidently get deleted or we would have to wipe data to start over with clean dummy data. I quickly realised this problem and created the *Data Manager* where we simply put in data in json format and pushed the button and viola we have data populated in the database. But, but, now we needed the dummy data and so I turned to *Mockaroo.com* where I created schemas to generate dummy data. Problem solved. See appendix Data Manager and Mockaroo. One another thing was to have user profile picture for that I used <https://randomuser.me/> to link dummy profile to dummy profile picture.

*Realisation*

We as students know that we need a better student portal. We didn’t want to just deliver some wireframes and dummy portal which did not work. In order to show our capabilities, we strived to create a student portal which actually worked. All of our components are database driven. I played a huge role in developing many of components.

Email preview connects into *Microsoft 365* account. In order to achieve this, I had to setup an application with *Microsoft Graph* so that our student portal can access logged in users emails which can fetch a preview on dashboard showing student their email.

Assignment submission was another item on our user stories. This had to be done on backend server and since our application connected directly to Firebase system, we didn’t need a backend system to begin with. I decided to create a backend just so the assignments could be uploaded. This however was not enough because the student also wants to see that their assignment is uploaded and details of it. This took creation of yet another Vue component called *DownloadAssignmentKeith*.vue which can detect which module page student is on and then load all the assignments uploaded by that user student. I didn’t stop there as I added two areas which would show *feedback* and *grade* received by the teacher. Now it was complete circle.

Login system seems something that is granted but to have a user authentication system where user session is kept can become complex. Having user authentication system was not really sought out for but we could not have a student portal work if there was no student logged in! So I created login system which did authenticate student by email and password. I did not go after encryption and secret tokens as this is not what the aim was for this prototype. Login works with nice validation colours and error messages, and it is good.

For students there are always questions and that means they need a medium where they can put their questions on. For this reason, I created *Question Chat box* where students can talk to their peers. This chat box is available at bottom on every box on module page so that the conversation is highly focused on the item right over it. Having used a single chat box does not work because it leads to a lot of explaining and confusion specially if students are all asking different questions. The realisation of this idea lead me to create something really practical and innovative.

Saxion has a lot of international students outside of Netherlands. This means sometimes students wants to remove the confusion and see the website in language which they better understand. Some of the colleges with advance student portal has feature to change language. Knowing 4 languages myself I’m always interested to create product which the user can use easier. This meant I decided to add in language package *Vue-i18n*. The package itself does not provide any translation because it doesn’t know which words you want to translate. In order to translate the words on the website I created language translation system with *Google Sheets* but this was not enough because now the translated words needed to be converted into a json format. For this I used Google sheets script editor to write code in JavaScript to produce valid json output which we inserted into the translation.js file. Viola and suddenly our portal became multilingual, 7 languages to be exact. The last issue was now the user interface to give student ability to see that they have this option, so I decided upon a flag icon system by using *vue-flag-icon package*. The process is fully automated which means we can add another language with just few keystrokes. See Appendix *Json Translation* and *Json Generator*

*Manage & Control*

In order to manage my and team’s time. I created timesheet where everyone can keep track of what tasks they are doing and how much time they have spent doing them. This is important because the client needs to know this information. These tasks are linked to the user stories where I colour coded each user story as per the *Moscow framework*. This report also calculated how many hours have been put into which user story and how important that user story is. This ensures that group member can determine whether he/she has spent too much time on something that is of less priority.

In order to make sure that group members are devote their committed time slots I decided to set daily group work on *Discord* channel. We all shared our screens with audio so that we could work as if we are sitting next to each other. This made it extremely easy to quickly get and give help to group members. Finally, this confirmed that all teammates are present during this time slot and actively focused progressing with the project. This initiate also made my efforts visible to the teammates seeing me motivated them to join in for combined work sessions.

In this project I was the appointed team lead by group members simply because they know that I know how to move the project in right direction. Being in such position is never easy because the leader has to make a lot of decisions and sometimes those decisions turn out to be not the best. Evaluating and managing the team members in friendly yet assertive manner I was able to create a working environment where team members became more motivated to work towards the project and really focus on the tasks, they themselves chose to take on. This meant that I would have to take other left-over tasks and make sure that all angles are covered in case team members did not spot them.

We used scrum process of daily finding out on who is working on what and what difficulties they are having so that we can help each other. The software development is large task and team members need to know exactly what they need to do so that they can get on with it. I was able to provide this clarity via verbal communication. Tools such as *GitLab* issues are there but they do not speak language of the humans as it is another box to fill in and it is ineffective. Teamwork is done via working closely with team members and I am quite satisfied how I managed the team.
## Tuan
Competence Choice 1:  Realization

- Motivation: During project, I spent almost half of the time learning and implementing new things that I learnt. I learnt and do *Vue* at the same time to keep my progress in track and catch up with other teammates. I was also able to build data trees and fetch the data from the firebase database. I also learnt how to connect back-end with front-end and doing the navigation between functions. I was basically able to connect everything together.
- Product: JavaScript, HTML, CSS, Vue, Vuetify, Firebase real-time database.
- Time: As I mentioned above, I spent almost half of the time each week so I would say I spent about 10-15 hours per week for this.  

Competence Choice 2: Design

- Motivation: I have a lot of wireframes and front-end knowledge, so I know my strength and focused on the design of the web we made. During the first sprint, when we brainstormed the idea for our web, I made some design and sketch out how the app we want to look like, even though the result web is far prettier than what we have in the wireframes, the format and structure of the web was based on it to be made. During other sprints, I made all components and design how they look with *Vuetify*, and 4 out of all were chosen to be in the official product that we will hand in. Basically, I do the design in almost every sprint and contributed to the look of the web quite a lot.
- Product: I used mockflow called WireframePro, as well as draw.io for the wireframes and diagrams. For the front-end, I use mostly Vuetify and a bit CSS, HTML, Javascript.
- Time: It can be seen obviously in my timesheet on how much time I spent on design. As I checked, it is around 10 hours every week, some week the hours were even 20 as we had to rush for the deadlines.

Competence Choice 3: Advise

- Motivation: As I have a brief research skills, during sprint 0 I was finding all over the internet the best platform and plugins to make the webapp, I came up with 3 options: Android with SpringBoots, Visual Code Studio with *Vue* or IntelliJ with Vue. After discussion with teacher, I advised my teammates to use IntelliJ with Vue framework as we can see our work live while writing the code and also Vue is a convenient tool for IT students and we really wanted to learn it.
- Product: As mentioned above, I advise the team to use Vue, and for front-end we were recommended *Vuetify* and I had soe research and learning about that after to check if it is the right tool for us, and I was amazed with it so I advised other team member to also use *Vuetify* instead of writing too much CSS. Besides, during sprint 3 where we had to decide on a database to fetch data from, at first, we were going to use SQLite and make API calls, but I found that unnecessary and SQLite has a lot of bugs, so I recommended everyone to use Firebase instead. I had some experience with Firebase Realtime Database from previous project so I was confident it will work well with our web. And the result should say it all.
- Time: For researching, I spent about 4-5 hours per week for that, except sprint 0, when we had to brainstorm the idea, I spent 15-20 hours each week looking for the right materials and Vue learning. 


## Sefanja
*Competence Choice 1: Design*

Motivation
In sprint 0 and sprint 1 I focused on the design of the website. This included all the concept wireframes which are in this document. In our meetings with our client, I would show her what we came up with and how I had adjusted the wireframes according to her feedback. I have also designed some of the components that we used in our website, some that we did use and some that we decided not to use as we had multiple concepts from our teammates.

Product
For the wireframes I first started using online tools such as “draw.io” and “mock flow”. These worked alright but I thought that it would be better if I could use a more interactive wireframe to show to the client. I did some research into this and ended up finding the program “*Adobe XD*”. With this tool I was able to build more detailed wireframes which you can see in the final concept.
*Vue(tify)* was used to design the components. 

Time
The wireframing design took me about 25 hours as I was focused on that in sprint 0 and a bit in sprint 1. Making components with *Vue* took 20 hours a week which was from sprint 1 to 3.

*Competence Choice 2: Analyses*

Motivation
As I was the person in charge of changing the design based on the client’s requirement, I had to analyse what would be the best way to implement this in our design. I listened to both my team’s suggestions as the client’s suggestions till everyone agreed upon a design I came up with. This also was the case when making the components. When a teammate had a better idea on how to do something or had an improvement, I would analyse what would be the best way to make that idea work.

Product
The products used were “draw.io”, “mock flow’, “Adobe XD”, HTML, Vue, JavaScript and real time firebase database.

Time
10 hours a week.

*Competence Choice 3: Realization*

Motivation
I have made multiple components some that we ended up using and some that we did not. I have also made them functionable and connected to the database. Some of these components that did make the final website are subjects, grades, skills passport and schedule. 

Product
Vue, JavaScript, HTML and real time firebase database were used.

Time
20 hours a week but 35 hours in the final week.

Appendices
## Database
### *Draft database structure*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.054.png)

Link: [https://lucid.app/lucidchart/24f1bea3-fb2b-4ef0-a532-1b9d5b9c11be/edit?page=0_0#](https://lucid.app/lucidchart/24f1bea3-fb2b-4ef0-a532-1b9d5b9c11be/edit?page=0_0) 
### *Actual Database*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.055.png)


### *Data Manager*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.056.png)





### *Mockaroo*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.057.png) 


## Translation
### *Word Translator*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.058.png)

### *Json Generator*
![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.059.png)


## Meetings
### *Questions for client*
- What kind of inspiration is available? What has been thought of already. Who is working on it?
- Do we need acceptance testing, user testing, software testing?
- Does it need to have functionalities like blackboard assignment receiving or is it for attending class?
- Are there any forbidden things? (Example: Saxion doesn’t want to be able to force students webcams on)
### *Client Meetings*
26 Apr 2021

- Create survey

28 Apr 2021

- Use research cards (<https://www.cmdmethods.nl/> ) / Design thinking
- Presentation on 29 June for client / prototype
- Web Application of my schedule

11 May 2021
**
`       `-      Client asked to check POA process

`       `-      POA form should be ready for meeting on Wednesday

19 May 2021

- Client showed us Coursera website as an example how the online classes look like on that website to give us some ideas.

26 May 2021

- Client asked if we could show our project to first year students because they are not happy with the current system.

2 Jun 2021

- Client would like us to implement a competences fragment where student will be able to see at what level their competences are.

9 Jun 2021

- Client wanted a progress bar for each course so that clients can see how far they are in the course.

21 Jun 2021

- Client wanted all the components together so that for the next meeting it will be navigational through every page.

25 Jun 2021

- We gave demo to the teacher and client. They seemed satisfied with demo of our new student portal prototype.
##

## ICT Card: Survey
Target number of questions 7 questions. Keep it close ended I.e multiple choice questions.

Survey link: <https://docs.google.com/forms/d/1kasEtMpxj8tp0HyVemiCodY7BiDkjBaCLkntGvzWHGs/edit>

![](Aspose.Words.053046f0-482f-4bea-b1b7-3d7610cc1b93.024.png)

- Which self-studying helping features/functions do you want Blackboard to have? - Tuan Nguyen
  - Self-made schedule
  - Exam/Class reminders
  - Daily quiz
  - Multiple choice sample tests before exams
  - Other: ...
- What do you think about the overall distance education of Saxion? - Sefanja
  - Poor
  - Below Average
  - Average
  - Good
  - Excellent
- Do you enjoy learning online? - Sefanja
  - Yes
  - It is all right.
  - No
- In terms of online lectures, what do you think can improve in this environment? (Multiple answers can be selected) - Sefanja
- what do you think can be improved in Saxion’s learning environment?
  - Recordings
  - Interactivity
  - Quality of online lecture
  - Chat
  - Online whiteboard
  - Webcam functions
  - Share files functions
  - Screenshare functions
- What is wrong with blackboard - Keith
  - Nothing is bad.
  - Bad design.
  - Hard to navigate.
  - Everything is bad.
- Do you prefer online learning or offline learning – Keith
  - Online
  - Offline
  - Either is fine
  - Why?
- During online lectures, do you prefer to see other people via their cam – Keith
  - Yes
  - No
- Are you satisfied with the current digital education environment (blackboard)? - Tuan
- ` `Yes
- ` `No
- Why yes/no?
- Which the following aspects of blackboard do you think needs to be improved? -Sefanja
- - Schedule 
- -course module
- -exam module
- -message module (teacher's feedback, Course announcement)

- Is it necessary to add some reminder functions to the blackboard, such as deadline reminder, class reminder and exam reminder?        - Tuan
- Yes
- No
- Do you think it is a clever idea to change the blackboard homepage to a page that supports personalized editing (you can change the position, size, and color of each module)?    -Sefanja
- Yes
- No
- Have you ever experienced interruption while taking exams/quizzes online via Blackboard? *Jane*
  - Very often
  - Often 
  - Not too often
  - Never
- Which of the following is the main reason for causing drawbacks of using Blackboard? *Jane*
  - Blackboard UI sucks, mobile version not fully supported.
  - Teachers being unorganized and/or inconsistent with the use Blackboard.
  - Trouble with connections, experience interruptions when using Blackboard Collaborate.
  - Layout is confusing for locating the necessary modules, and too much unnecessary things on one page.
- Which of the following features do you used the most on Blackboard, besides looking at class materials/syllabi? *Jane*
  - Watch previously recorded lectures.
  - Check grades. 
  - Use discussion board.
  - Check class roster.
  - Communicate and collaborate with team members.
  - Get feedback from teachers on submitted assignments.
- Which of the following LMS have you ever heard of besides Blackboard? *Jane*
  - Canvas
  - Moodle
  - Edmodo
  - Google Classroom
  - None of the above


# Resources
Studiedata

<https://doe-meer-met-studiedata.nl/wat-betekent-dit-voor-mij/>

Levels of competences

<https://hbo-i.nl/domeinbeschrijving/> 

Todo applications

<https://todomvc.com/>

Som Model English

<https://som-eng.digitaal-magazine.nl/eng-som-2-0/educational-philosophy>  

Final Survey link:

<https://docs.google.com/forms/d/1kasEtMpxj8tp0HyVemiCodY7BiDkjBaCLkntGvzWHGs/edit>

GitLab Repository

[https://gitlab.com/saxion.nl/hbo-ict/2.4-project /student-elo](https://gitlab.com/saxion.nl/hbo-ict/2.4-project/student-learning/activity)

Json Designer

<http://www.objgen.com/json/models/bnEc> 

Json Validator

<https://jsoneditoronline.org/#left=local.qibiwo&right=local.hamude> 

Json Escape

<https://www.freeformatter.com/json-escape.html> 

HTML minify

<https://www.textfixer.com/html/compress-html-compression.php> 

Randon user generator

<https://randomuser.me/>

Mockaroo Dummy data

<https://www.mockaroo.com/>

Microsoft graph

<https://developer.microsoft.com/en-us/graph>

Weather

<https://www.weatherbit.io/>

Flag icons

<https://www.npmjs.com/package/vue-flag-icon>

Apexcharts

<https://apexcharts.com/>




# References
We used following addons in our project.

File upload

<https://bezkoder.com/vuetify-file-upload/>

<https://bezkoder.com/node-js-express-file-upload/>

Notes 

<https://vuejsexamples.com/an-awesome-tool-for-retrospecting-written-in-vue/>

Vuetify

<https://vuetifyjs.com/>

Vue

<https://vuejs.org>

Smart World

<https://codepen.io/qpi65/pen/LYNOXJO>

Translation package

<https://www.npmjs.com/package/vue-i18n>



|||
| - | - |
|` `*PAGE   \\* MERGEFORMAT 14*|<p>*Tuan N., Sefanja H., Keith I.*</p><p></p>|

