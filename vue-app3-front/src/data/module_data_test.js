export default {
  module_code: 'sx12local-testData',
  module_name: 'Introduction to Programming',
  weekPages: [{
    top_menu_name: 'Overall',
    submenu: [{
      name: 'Announcements',
      icon: 'mdi-bullhorn'
    },
    {
      name: 'Instructions',
      icon: 'mdi-book-open-blank-variant'
    },
    {
      name: 'P2P Study',
      icon: 'mdi-forum'
    }
    ],
    submenu_items: [{
      tasks: [{
        title: 'Official results test Introduction to Programming',
        subtitle: '(January 12th, 2021 - retake)',
        content: '',
        questions: [{
          student_name: 'Ede S.',
          student_id: '475271',
          comment: ' how does this while loop work.',
          dateTime: 1624470795000
        },
        {
          student_name: 'Tiebold B.',
          student_id: '471918',
          comment: ' i dont get it.',
          dateTime: 1624470795000
        },
        {
          student_name: 'Carolina B.',
          student_id: '481733',
          comment: ' what lol ',
          dateTime: 1624470795000
        }
        ]
      }]
    },
    {
      tasks: [{
        title: 'Course Manual',
        subtitle: '',
        content: ''
      }]
    },
    {
      tasks: [{
        title: 'Ask your question here',
        subtitle: '',
        content: ''
      }]
    }
    ]
  },

  {
    top_menu_name: 'Week 1',
    submenu: [{
      name: 'Materials',
      icon: 'mdi-briefcase'
    },
    {
      name: 'Videos',
      icon: 'mdi-video'
    },
    {
      name: 'Exercises',
      icon: 'mdi-book-open-variant'
    },
    {
      name: 'Exercises',
      icon: 'mdi-book-open-variant'
    },
    {
      name: 'Exercises3333',
      icon: 'mdi-book-open-variant'
    }
    ],
    submenu_items: [{
      tasks: [{
        title: 'Getting started',
        subtitle: '',
        content: ''
      },
      {
        title: 'Prepare',
        subtitle: '',
        content: ''
      }
      ]
    },
    {
      tasks: [{
        title: 'Installing Instruction',
        subtitle: '',
        videoId: 'cu9-chtteAg'
      }]
    },
    {
      tasks: [{
        title: 'Exercise 1',
        subtitle: 'basics',
        content: ''
      },
      {
        title: 'Exercise 2',
        subtitle: 'intermediate',
        content: ''
      }
      ]
    },
    {
      tasks: [{
        title: 'Exercise 1',
        subtitle: 'basics',
        content: ''
      },
      {
        title: 'Exercise 2',
        subtitle: 'intermediate',
        content: ''
      }
      ]
    },
    {
      tasks: [{
        title: 'Getting started',
        subtitle: '',
        content: ''
      },
      {
        title: 'Prepare',
        subtitle: '',
        content: ''
      }
      ]
    }

    ]
  }

  ]
}
