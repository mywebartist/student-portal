[{
  module_code: 'SX12',
  weekPages: [{
    top_menu_name: 'info',
    submenu: [{
      name: 'Announcements',
      icon: 'mdi-help-circle'
    },
    {
      name: 'instruction',
      icon: 'mdi-help-circle'
    }
    ],
    submenu_items: [{
      tasks: [{
        title: 'Official results test Introduction to Programming',
        subtitle: '(January 12th, 2021 - retake)',
        content: "<div class=\"vtbegenerated\"> <p>Hello students (that participated in the retake of an Introduction to Programming)!</p> <p>In a few moments the final grades for Introduction Programming retake of January 12th 2021 will be published via Bison. For most of you the results will not be a surprise as the individual rubric scores have been on Blackboard for some time now.</p> <p>We would like to ask all students to check if we have entered your grade correctly based on the number of points. Blackboard is leading in this, so if your grade somehow is incorrect, please contact me (Tristan, t.pothoven@saxion.nl) as soon as possible.</p> <p>As far as the exam review is concerned, Introduction Programming will *not* make use of the scheduled exam review time in quartile 3. This is because your grade is already explained via Blackboard including our arguments.</p> <p>It is of course possible that you have questions about your result or that you suspect an error has been made somewhere. You can report this by sending me (Tristan) an email.&nbsp;</p> <p>I do ask however that you provide the following information:</p> <ul> <li>Which rubric line is it about? (functionality, data types, methods, if-statements, loops or lists)</li> <li>What is wrong with it? (unclear reasoning from us, too few points for reason X, etc.)</li> </ul> <p>(If you have questions on more than one topic, I would kindly request that you write this out per topic.)</p> <p>General remarks such as \"I disagree with the result\" without any further argumentation unfortunately cannot be discussed. So please be as explicit as possible.</p> <p><span style=\"color: #000000; font-family: 'Open Sans', sans-serif; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">You will have 2 weeks to respond to your result in case of questions. This means that the official period of \"exam review\" now has begun and lasts until February 17th, 2021 after which all results are final.</span></p> <p>If you have any questions about the test or your grade, please feel free to send me an e-mail!</p> </div>"
      }]
    },
    {
      tasks: [{
        title: 'Course Manual',
        subtitle: 'SETUP GUIDE',
        content: "<div class=\"vtbegenerated\"> <p>Every IT-professional, at some point in his career, is most likely to encounter a process called <i>software development</i>. We consider the development of software to contain both the creation of new and the adjustment / personalization of existing software. Software development is usually done by a group of people called <i>software engineers</i>.&nbsp;</p> <p>Regardless of whether you strive to be a software engineer yourself, you are most likely to co-operate with them as a professional. And, to be able to communicate with them, you need to learn to speak their language. In this course we’re going to make a first effort in learning to write code, so that you have a better understanding of what it is to be a software engineer.</p> <p>During this course you’ll get acquainted with basic programming concepts, such as <i>variables</i>, <i>conditions</i>, <i>lists</i> and <i>loops</i> by accumulating some practical experience with programming (in Java). After this course you are able to write your own (small) Java programs!</p> <p>This course is taught using the Flipped Classroom method. This means that you are asked to prepare materials (study some material, do some assignments, etc.) in advance of the meeting.</p> <p>During this course we'll use a slightly different group-assignment than other courses. Please check the Announcements page for more information. (You need have signed in to this course to be able to read the announcements.)</p> <p>Later this quartile, we'll set up a Discord server that you can use to ask programming related questions</p> <p><br></p> <p><span style=\"text-decoration: underline;\">For this course we're using a slightly different group assignment than used in mentor groups. Please check out the first announcement on the \"Announcements\" page.</span></p> </div>"
      }]
    }
    ]
  },
  {
    top_menu_name: 'week 1',
    submenu: [{
      name: 'week 1 assignment 1',
      icon: 'mdi-book-open-blank-variant'
    },
    {
      name: 'week 1 assignment 2',
      icon: 'mdi-book-open-blank-variant'
    }
    ],
    submenu_items: [{
      tasks: [{
        title: 'week 1 assignment 1a',
        subtitle: 'basics',
        content: "<div class=\"vtbegenerated\"> <table border=\"0\"> <tbody> <tr> <td width=\"100px\" align=\"center\" valign=\"top\" style=\"font-size: 32px; line-height: 20px; color: #8ec030;\">★<span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\"><span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">✩</span></span><span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">✩</span></td> <td> <div class=\"vtbegenerated_div\">Write a program that prints someones full name.</div> </td> </tr> <tr> <td>&nbsp;</td> <td> <div class=\"vtbegenerated_div\">For example:</div> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\">// In code: String lastName = \"Hageraats\" <br>// in code: String firstName = \"Esther\" <br>Your full name is Esther Hageraats<span color=\"#008000\" style=\"color: #008000;\"><i><br></i></span></pre> </td> </tr> </tbody> </table> </div>"
      },
      {
        title: 'week 1 assignment 1b',
        subtitle: 'intermediate',
        content: "<div class=\"vtbegenerated\"> <table border=\"0\"> <tbody> <tr> <td width=\"100px\" align=\"center\" valign=\"top\" style=\"font-size: 32px; line-height: 20px; color: #8ec030;\">★<span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\">★</span><span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\"><span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: center; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">✩</span></span></td> <td> <div class=\"vtbegenerated_div\">Write a program that creates 9 variables and prints the value of the variables on the right location of the gameboard.</div> <div class=\"vtbegenerated_div\">Tip: Choose a variable type that can hold '1', '2', 'X' and 'O'.</div> </td> </tr> <tr> <td>&nbsp;</td> <td> <div class=\"vtbegenerated_div\">For example:</div> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\"> 1 | 2 | 3<br>-----------<br> 4 | X | O<br>-----------<br> 7 | 8 | 9</pre> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\"><span style=\"color: #000000; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\">Or</span></pre> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\"> X | 2 | O<br>-----------<br> 4 | 5 | 6<br>-----------<br> 7 | X | O</pre> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\"></pre> </td> </tr> </tbody> </table> </div>"
      }
      ]
    },
    {
      tasks: [{
        title: 'week 1 assignment 2a',
        subtitle: 'practice',
        content: "<div class=\"vtbegenerated\"> <table border=\"0\"> <tbody> <tr> <td width=\"100px\" align=\"center\" valign=\"top\" style=\"font-size: 32px; line-height: 20px; color: #8ec030;\">★<span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\">★</span>✩</td> <td> <div class=\"vtbegenerated_div\">Write a program that prompts the user for the current day and responds with a reaction to that day. You may define your own responses.</div> </td> </tr> <tr> <td>&nbsp;</td> <td> <div class=\"vtbegenerated_div\">For example:</div> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\">What is the current day? <em><span style=\"color: #008000;\">Monday</span></em>Aaaaaaaaaaaaaaaaaaaargh!</pre> <div class=\"vtbegenerated_div\">And:</div> <pre style=\"display: block; font-family: monospace; white-space: pre; margin: 1em 0; font-size: 12pt;\">What is the current day? <em style=\"font-size: 16px;\"><span style=\"color: #008000;\">Friday<br></span></em>It's almost weekend!</pre> </td> </tr> </tbody> </table> </div>"
      },
      {
        title: 'week 1 assignment 2b',
        subtitle: 'more practice',
        content: "<div class=\"vtbegenerated\"> <table border=\"0\"> <tbody> <tr> <td width=\"100px\" align=\"center\" valign=\"top\" style=\"font-size: 32px; line-height: 20px; color: #8ec030;\">★<span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\">★</span><span style=\"color: #8ec030; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 32px; font-style: normal; text-align: center;\">★</span></td> <td> <div class=\"vtbegenerated_div\">Write a program that calculates the amount of change (and corresponding coins/bills) given a certain purchase. First you should prompt for the amount due and amount paid. There are no bills / coins larger than 100 euro present in the store.</div> </td> </tr> <tr> <td>&nbsp;</td> <td> <div class=\"vtbegenerated_div\"><span style=\"font-family: monospace; font-size: 12pt; white-space: pre;\">Amount due (in cents):<em style=\"font-family: monospace; font-size: 12pt; white-space: pre;\"><span style=\"color: #008000;\">5215<br></span></em>Amount paid (in cents): <em><span style=\"color: #008000;\">10000<br></span></em>Change: 4785<br>100 euro: 0<br>50 euro: 0<br>20 euro: 2<br>10 euro: 0<br>5 euro: 1<br>2 euro: 1<br>1 euro: 0<br>50 cent: 1<br>20 cent: 1<br>10 cent: 1<br>5 cent: 1</span></div> </td> </tr> </tbody> </table> </div>"
      }
      ]
    }
    ]
  }
  ]

}
]
