{
  "module_code": "SX12",
  "topmenu": [
    {
      "menu_name": "intro",
      "submenu": [
        {
          "submenu_name": "introduction",
          "tasks": [
            {
              "title": "introduction",
              "subtitle": "level 1",
              "content": "welcome wij gaan beginnen"
            },
            {
              "title": "course manual",
              "subtitle": "welcome wij gaan beginnen",
              "content": "copy past is golden"
            }
          ]
        }
      ]
    },
    {
      "menu_name": "week 1",
      "submenu": [
        {
          "submenu_name": "assignment",
          "tasks": [
            {
              "title": "How to install the Java Development Kit and IntelliJ IDEA Community Edition",
              "subtitle": "Set up system",
              "content": "During the first lecture we will install the required software together, but in case you are wondering we have created a step-by-step guide on how to install the software needed for this course. Head over to https://www.oracle.com/nl/java/technologies/javase-downloads.htmland go download the Java SE 11 (LTS) installer for your specific operating system. You'll need to create an Oracle account and sign in to be able to download the installer. Head over to https://www.jetbrains.com/idea/download and get IntelliJ IDEA Community Edition for your specific operating system. Install the Java SE 11 development kit downloaded from step 1. Unfortunately, there is no \"easy\" way to check whether or not the installation was a success. If the installer exitedwithout errors, you should assume everything is up and running. If your installer crashes, please contact your teacher. Install IntelliJ IDEA Community Edition downloaded from step 2. Download the DemoProject.zip file (below) and extract it to somewhere you can easily find it. Open IntelliJ and select open and select the folder you extracted from the zipfile as discussed in step 5. Head over to \"1: Project\" (top left), open up \"Demo1\", open up \"src\" before double-clicking on \"Application\". You might need to wait before IntelliJ has indexed the Java Development Kit. If you see a progressbar in the bottom, wait a moment! Be patient. If everything worked out correctly, you should a green triangle that you can now click to run your program. If you can do so, your installation was succesful! Just to help out, we have created a video that shows all these steps and explains a little about what happens. Have a look at https://youtu.be/8Dp9jP56b4U."
            },
            {
              "title": "Sandbox project",
              "subtitle": "level 1",
              "content": "Download the Sandbox project here. A Sandbox project does not contain exercises, but should be considered a playground for you to experiment upon!"
            }
          ]
        }
      ]
    }
  ]
}