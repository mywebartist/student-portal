# A Student Friendly ELO

![](screenshots/smart.png)

## Demo
[https://studentportal.mywebartist.eu/](https://studentportal.mywebartist.eu/)


### How to run Frontend
1. run `npm install` in 
`vue-app3-front` directory
2. run `npm serve`
3. goto [http://localhost:8080](http://localhost:8080)

### How to run Backend
1. run `npm install` in `vue-app3-back` directory
2. run `npm serve`
3. goto [http://localhost:3002](http://localhost:3002)

### Documentation
[readme_doc/README.md](readme_doc/README.md)


